### Raison d'etre ###

This script finds by enumeration, distinct sets of numbers, formed from subtracting two different 3 digit numbers, formed by subtracting any 2 permutations of the 3 digits of any of a third set of 3 digit numbers, ABC that all have the same length gaps between consecutive digits, namely (C-B) and (B-A) if C>=B>=A.  These characteristic gaps can be used to generate the set of numbers, and so make convenient names for these sets or Chords.  I wanted to see if I had missed any numbers, and calculate them for any number-base system (not just 10).  I have not found any description of these 16 sets of numbers yet, despite an extensive search of the literature (5 minutes on wikipedia), but would welcome such information.
* Version 1.0.0 Super-alpha

### How do I get set up? ###

A) Run Locally:

Copy Calc_chord_name_pairs.m to desired location
Change directories to desired Location in GNU_Octave (or set path there)
type `Calc_chord_name_pairs(10)' and press enter to find numbers for base 10

B) Run in cloud:
Upload Calc_chord_name_pairs.m to http://octave-online.net/#
or sign in and use shared workspace at 
http://octave-online.net/?s=YOUWPCEPvfjIJyjsQauxZiBeCiWcoliHDAfSdMWaBcitSqrd

No responsibility is implied or accepted by the contributors for anything that happens on this or any external website.