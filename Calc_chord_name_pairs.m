function bhuio=Calc_chord_name_pairs(base)
%digits will be expressed internally in base 10 in Octave by default on output but principle is sound in Hexadecimal etc.
if base < 3+1
	cstrcat("Not enough non-zero digits in base ",num2str(base)," number system to pick 3 from")
	return;
endif
Chord_names=[]; 
Chord_numbers=[];
Chord_digits=[];
for i=0:(base-1-3) %  i <= base-3-1 Otherwise there are no more digits left to pick three from {0,1,...,base-1}.
	for j=i:(base-3-1-i) % w.l.o.g.  i <= j, and j must be small enough s.t. there are 3 digits left to pick in 
	% {0,1,...,base-1}.  [i;j] is an ordered pair of digits, for which there exist possible ordered 
	%number_of_Chord_numberslets of 
	% unique digits [a,b,c] in {1,2,...,base-1} with c>b>a s.t. {b-a-1,c-b-1} = {i,j}                    
		if numel(Chord_names)>0
			%for each Chord, "the [i j] Chord"
			if numel(find(prod(Chord_names==[i;j])+prod(Chord_names==[j;i])))==0 %If [i;j] and [j;i] aren't in Chord_names
				Chord_names=[Chord_names,[i;j]];
				[i,j];
				[test_Chord_numbers,test_Chord_digits]=find_numbers_and_digits([i;j],base);
				if length(test_Chord_numbers)>size(Chord_numbers,2)
					Chord_numbers=[postpad(Chord_numbers,length(test_Chord_numbers),0,2);test_Chord_numbers];
					Chord_digits=[postpad(Chord_digits,size(test_Chord_numbers,2),0,2);test_Chord_digits];

				else
				    Chord_numbers=[Chord_numbers;postpad(test_Chord_numbers,size(Chord_numbers,2),0,2)];
				    Chord_digits=[Chord_digits;postpad(test_Chord_digits,size(Chord_digits,2),0,2)];
				endif				
			endif	
		else    %First Chord_name only
			Chord_names=[i;j];
			[Chord_numbers,Chord_digits]=find_numbers_and_digits([i;j],base);
			
		endif
	endfor
endfor
[Chord_names', Chord_numbers]
numel(Chord_names)/2;

function [three_digit_numbers,triplets_of_digits]=find_numbers_and_digits(chord_name,Base);
three_digit_numbers=[];
triplets_of_digits=[];
for first_digit=1:(Base-sum(chord_name)-3);
	second_digit=first_digit+chord_name(1)+1;
	third_digit=second_digit+chord_name(2)+1;
	test_number=[first_digit,second_digit,third_digit]*[Base^2;Base;1];
	if numel(three_digit_numbers)==0
		three_digit_numbers=[three_digit_numbers,test_number];
		triplets_of_digits=[triplets_of_digits,[first_digit;second_digit;third_digit]];
	else
		if numel(find(three_digit_numbers==test_number))==0
		   		three_digit_numbers=[three_digit_numbers,test_number];
				triplets_of_digits=[triplets_of_digits,[first_digit;second_digit;third_digit]];
		endif
	endif
	%
	second_digit=first_digit+chord_name(2)+1;
	third_digit=second_digit+chord_name(1)+1;
	test_number=[first_digit,second_digit,third_digit]*[Base^2;Base;1];
	if numel(find(three_digit_numbers==test_number))==0
		three_digit_numbers=[three_digit_numbers,test_number];
		triplets_of_digits=[triplets_of_digits,[first_digit;second_digit;third_digit]];
	endif
	
	%
	%
endfor
endfunction

endfunction